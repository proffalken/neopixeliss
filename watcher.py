import requests
import logging

from geopy.distance import great_circle

my_loc = (lat, lng)
status = "green"

logging.basicConfig(level=logging.INFO)


def get_satelites():
    sat_res = requests.get('https://api.wheretheiss.at/v1/satellites')
    sats = sat_res.json()
    for sat in sats:
        if sat["name"] == "iss":
            get_satelite_info(sat['id'])


def get_satelite_info(sat_id=None):
    global status
    sat_info_res = requests.get("https://api.wheretheiss.at/v1/satellites/%s" %
                                sat_id)
    sat_info = sat_info_res.json()
    sat_loc = (sat_info['latitude'], sat_info['longitude'])
    distance = great_circle(sat_loc, my_loc).miles
    if distance > 10000:
        status = "blue"
    elif distance > 8000:
        status = "purple"
    elif distance > 6000:
        status = "red"
    elif distance > 3000:
        status = "yellow"
    else:
        status = "green"

    api_url = "https://api.particle.io/v1/devices"
    access_token = 'PARTICLE ACCESS TOKEN'
    update = requests.post("%s/%s/setColour"
                           % (api_url, "PARTICLE DEVICE ID"
                              ),
                           data={'params': status,
                                 'access_token': access_token}
                           )
    logging.debug("Payload returned: %s" % update.content)
    logging.info("Colour Sent: %s " % status)
    logging.info("Distance from Current Location: %s" % distance)

if __name__ == "__main__":
    get_satelites()
