// This #include statement was automatically added by the Spark IDE.
#include "neopixel/neopixel.h"

#define PIXEL_PIN D7
#define PIXEL_COUNT 60
#define PIXEL_TYPE WS2812B

Adafruit_NeoPixel strip = Adafruit_NeoPixel(PIXEL_COUNT, PIXEL_PIN, PIXEL_TYPE);

void setup() {
  strip.begin();
  strip.show();
  Particle.function("setColour", setColour);
  strip.setBrightness(64);
}

void loop() {

}

int setColour(String colour) {
  Particle.publish(colour);
  if (colour == "blue") {
    for(int p = 0; p < PIXEL_COUNT/5; p++)
        {
              strip.setPixelColor(p, 0, 0, 255, 0);
        }
  } else if (colour == "purple") {
    for(int p = 0; p < PIXEL_COUNT/4; p++)
        {
              strip.setPixelColor(p, 150, 0, 200, 0);
        }
  } else if (colour == "red") {
    for(int p = 0; p < PIXEL_COUNT/3; p++)
        {
              strip.setPixelColor(p, 255, 0, 0, 0);
        }
  } else if (colour == "yellow") {
    for(int p = 0; p < PIXEL_COUNT/2; p++)
        {
              strip.setPixelColor(p, 255, 255, 0, 0);
        }
  } else if (colour == "green") {
    for(int p = 0; p < PIXEL_COUNT; p++)
        {
              strip.setPixelColor(p, 0, 255, 0, 0);
        }
  }
  strip.show();
}


